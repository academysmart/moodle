<?php
/**
 * Created by PhpStorm.
 * User: jurets
 * Date: 9/12/2017
 * Time: 10:34
 */

if ($hassiteconfig) {
//new \admin_settingpage('local_o365', new lang_string('pluginname', 'local_o365'));
//$ADMIN->add('localplugins', $settings);

    $settings = new admin_externalpage(
        'localsmart',
        get_string('smart', 'local_smart') . ': ' . get_string('usermanager', 'local_smart'),
        "$CFG->wwwroot/local/smart/usermanager.php"
    //,'report/courseoverview:view',
    // empty($CFG->enablestats)
    );
    $ADMIN->add('localplugins', $settings, 'local_o365');
}