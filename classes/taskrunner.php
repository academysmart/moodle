<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Smart Local Plugin
 *
 * @package    local_smart
 * @author     Shilling <mitsserhiy@gmail.com>, Jurets <jurets75@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2017 onwards Mits Serhiy (mitsserhiy@gmail.com), Yurii Hetmanskyi (jurets75@gmail.com)
 */

namespace local_smart;

class taskrunner
{
    private static $valid_tasks = ['\local_o365\task\usersync', '\local_smart\task\userfilter'];

    public static function run($task)
    {
        global $CFG, $DB;

        if (!in_array($task, self::$valid_tasks)) {
            mtrace('Error! Forbidden task');
        } else {
            $task = \core\task\manager::get_scheduled_task($task);

            echo '<pre>';
            if ($task) {
                // Increase time and memory limits.
                \core_php_time_limit::raise();
                raise_memory_limit(MEMORY_EXTRA);

                $predbqueries = $DB->perf_get_queries();
                $pretime = microtime(true);

                $fullname = $task->get_name() . ' (' . get_class($task) . ')';
                mtrace('Execute scheduled task: ' . $fullname);
                // NOTE: it would be tricky to move this code to \core\task\manager class,
                //       because we want to do detailed error reporting.
                $cronlockfactory = \core\lock\lock_config::get_lock_factory('cron');
                if (!$cronlock = $cronlockfactory->get_lock('core_cron', 10)) {
                    mtrace('Cannot obtain cron lock');
                    //exit(129);
                } else if (!$lock = $cronlockfactory->get_lock('\\' . get_class($task), 10)) {
                    $cronlock->release();
                    mtrace('Cannot obtain task lock');
                    //exit(130);
                } else {
                    $task->set_lock($lock);
                    if (!$task->is_blocking()) {
                        $cronlock->release();
                    } else {
                        $task->set_cron_lock($cronlock);
                    }

                    //// Run task
                    try {
                        get_mailer('buffer');
                        $task->execute();
                        if (isset($predbqueries)) {
                            mtrace("... used " . ($DB->perf_get_queries() - $predbqueries) . " dbqueries");
                            mtrace("... used " . (microtime(1) - $pretime) . " seconds");
                        }
                        mtrace('Scheduled task complete: ' . $fullname);
                        \core\task\manager::scheduled_task_complete($task);
                        get_mailer('close');
                        //exit(0);
                    } catch (Exception $e) {
                        if ($DB->is_transaction_started()) {
                            $DB->force_transaction_rollback();
                        }
                        mtrace("... used " . ($DB->perf_get_queries() - $predbqueries) . " dbqueries");
                        mtrace("... used " . (microtime(true) - $pretime) . " seconds");
                        mtrace('Scheduled task failed: ' . $fullname . ',' . $e->getMessage());
                        if ($CFG->debugdeveloper) {
                            if (!empty($e->debuginfo)) {
                                mtrace("Debug info:");
                                mtrace($e->debuginfo);
                            }
                            mtrace("Backtrace:");
                            mtrace(format_backtrace($e->getTrace(), true));
                        }
                        \core\task\manager::scheduled_task_failed($task);
                        get_mailer('close');
                        //exit(1);
                    }
                }
            } else {
                mtrace('Error! Task not found');
            }
            echo '</pre>';
        }
    }

}