<?php

/**
 * @package local_smart
 */
namespace local_smart\task;
global $CFG;

require_once( $CFG->dirroot . '/report/groupgrades/lib.php' );
require_once( $CFG->dirroot . '/lib/accesslib.php' );
require_once("$CFG->dirroot/mod/assign/locallib.php");

use assign;
use context_course;
use context_module;
use html_writer;
use moodle_url;

/**
 * Scheduled task to sending email to users about new assignment.
 */
class assignnotification extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('task_assignnotification', 'local_smart');
    }

    /**
     * Do the job:
     * Send email to users if new task was created/updated 3 hours ago.
     */
    public function execute() {
        $assignments = get_course_notification_data_for_cron();
        if (count($assignments)) {
            $subject = 'Повідомлення про створення нового завдання';
            global $DB;
            foreach ($assignments as $row) {
                $course = $DB->get_record('course', ['id' => $row->course_id], '*', MUST_EXIST);
                $users_in_course = get_enrolled_users(context_course::instance($course->id), '', 0);
                if (count($users_in_course)) {
                    $url = new moodle_url('/mod/assign/view.php', ['id' => $row->assignment_id]);
                    $assign = get_coursemodule_from_id('assign', $row->assignment_id, $course->id);
                    $context = context_module::instance($row->assignment_id);
                    $assignment = new assign($context, $assign, $course);
                    foreach ($users_in_course as $user) {
                        $due_date = $assignment->get_instance()->duedate;
                        $message = "Шановний(а) {$user->firstname} {$user->lastname}.\r\n" .
                                   "В системі LMS kmbs з'явилося нове домашнє завдання до виконання '{$assignment->get_instance()->name}' з курсу {$course->fullname}.\r\n\r\n";
                        if (isset($due_date) && $due_date) {
                            $message .= "Дедлайн " . date('Y/m/d H:i', $due_date) . ".\r\n\r\n";
                        }
                        $message .= "Переглянути завдання можна перейшовши за посиланням: " . html_writer::link($url, 'перейти');
                        email_to_user($user, \core_user::get_noreply_user(), $subject, $message, $message);
                    }
                    update_course_notification($row->assignment_id, 'assign', $course->id, 1);
                    mtrace('Email was sent to ' . count($users_in_course) . ' users.');
                }
            }
        } else {
            mtrace('New assignments are not available');
        }
    }

}
