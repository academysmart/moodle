<?php

require($_SERVER['DOCUMENT_ROOT'] . '/config.php');

global $DB;

// get users with unauthorized e-mail addresses for deleting all of them from DB
$sql = "SELECT *
          FROM {user} u
         WHERE u.deleted = 0 AND u.auth = 'oidc' AND u.email NOT IN (
                                                 SELECT u.email
                                                   FROM {user} u
                                                  WHERE u.deleted = 0 AND (u.email LIKE '%@kmbs.ua' OR u.email LIKE '%@my.kmbs.ua')
                                                )";
$users = $DB->get_records_sql($sql);

$usersId = [];
foreach ($users as $user) {
    if (!is_siteadmin($user->id)) {

        // logical removal of records from DB
        //$DB->set_field('user', 'deleted', '0', ['id' => $user->id]);

        // delete user using moodle functional
        delete_user($user);
        $fff = $user->id;
    }
    $usersId[] = $user->id;
}


// physical removal of records from DB
// BE CAREFUL ! It can only be used for absolutely new (just loaded) users
//$DB->delete_records_list('user', 'id', $usersId);

var_dump($users);
var_dump($usersId);


// get users for role assignments
$sql = "SELECT u.id, u.email
          FROM {user} u
         WHERE u.deleted = 0 AND (u.email LIKE '%@kmbs.ua' OR u.email LIKE '%@my.kmbs.ua')";
$users = $DB->get_records_sql($sql);

// get student role id and system context id
$role = $DB->get_record('role', ['shortname' => 'student']);
$context = context_system::instance();

// process incoming role assignments
foreach ($users as $user) {
    if (strstr($user->email, '@') === '@kmbs.ua') {
        echo $user->id;
        echo '<br>';
        role_assign($role->id, $user->id, $context->id);
    }
}






