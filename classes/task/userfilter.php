<?php

/**
 * @package local_smart
 */
namespace local_smart\task;

/**
 * Scheduled task to filter users...
 */
class userfilter extends \core\task\scheduled_task
{

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name()
    {
        return get_string('task_userfilter', 'local_smart');
    }

    /**
     * Do the job:
     * remove users with invalid email addresses and
     * assign system roles for users with valid email addresses.
     */
    public function execute()
    {
        global $DB;

        // get users with unauthorized e-mail addresses for deleting all of them from DB
        $sql = "SELECT *
          FROM {user} u
         WHERE u.deleted = 0 AND u.auth = 'oidc' AND u.email NOT IN (
                 SELECT u.email
                   FROM {user} u
                  WHERE u.deleted = 0 AND (u.email LIKE '%@kmbs.ua' OR u.email LIKE '%@my.kmbs.ua')
                )";
        $users = $DB->get_records_sql($sql);

        // delete users using moodle functional
        $counter = 0;
        foreach ($users as $user) {
            if (!is_siteadmin($user->id)) {
                delete_user($user); $counter++;
            }
        }
        mtrace('Count of deleted extra users: ' . $counter);

        // get users for role assignments
        $domain_st = '@my.kmbs.ua';
        $domain_man = '@kmbs.ua';

        // get student role id and system context id
        $role = $DB->get_record('role', ['shortname' => 'student'], 'id');
        $context = \context_system::instance();

        $sql = "SELECT u.id, u.email
          FROM {user} u
         WHERE u.deleted = 0 AND (u.email LIKE :domain_man OR u.email LIKE :domain_st) AND NOT EXISTS (
            SELECT 1
            FROM mdl_role_assignments ra
            WHERE ra.contextid = :contextid AND ra.roleid = :roleid AND ra.userid = u.id
        )"; //LIMIT 5
        $users = $DB->get_records_sql($sql, [
            'domain_man'=>'%'.$domain_man,
            'domain_st'=>'%'.$domain_st,
            'contextid'=>$context->id,
            'roleid'=>$role->id,
        ]);

        // process incoming role assignments
        $counter = 0;
        foreach ($users as $user) {
            if (strstr($user->email, '@') === $domain_st) {
                role_assign($role->id, $user->id, $context->id);
                $counter++;
            }
        }
        mtrace('Count of Student Role assignments: ' . $counter);
    }

}
