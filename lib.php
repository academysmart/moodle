<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Smart Local Plugin
 *
 * @package    local_smart
 * @author     Shilling <mitsserhiy@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2017 onwards Mits Serhiy (mitsserhiy@gmail.com)
 */

/**
 * Add custom categories and nodes to myprofile page
 *
 * @param \core_user\output\myprofile\tree $tree Tree object
 * @param stdClass $user user object
 * @param bool $iscurrentuser
 * @param stdClass $course Course object
 * @return bool
 */
function local_smart_myprofile_navigation(core_user\output\myprofile\tree $tree, $user, $iscurrentuser, $course)
{
    global $DB;

    if (isguestuser($user)) {
        // The guest user cannot post, so it is not possible to view any posts.
        // May as well just bail aggressively here.
        return false;
    }

    // add new category for myprofile page
    $category = new core_user\output\myprofile\category('systemroles', '', null);
    $tree->add_category($category);

    // get roles of the user in the context of the system
    $sql = "SELECT ra.id, ra.userid, ra.contextid, ra.roleid, ra.component, ra.itemid, c.path, r.name, r.shortname, c.contextlevel
          FROM {role_assignments} ra
          JOIN {context} c ON ra.contextid = c.id
          JOIN {role} r ON ra.roleid = r.id
         WHERE ra.userid = ? AND c.contextlevel = ?
      ORDER BY contextlevel DESC, contextid ASC, r.sortorder ASC";
    $roles_array = $DB->get_records_sql($sql, array($user->id, CONTEXT_SYSTEM));
    $roles = '';

    foreach ($roles_array as $ra) {

        if (trim($ra->name) !== '') {
            // For filtering always use context where was the thing defined - system for roles here.
            $original = format_string($ra->name, true, ['context' => context_system::instance()]);
        } else {
            // Empty role->name means we want to see localised role name based on shortname,
            // only default roles are supposed to be localised.
            switch ($ra->shortname) {
                case 'manager':
                    $original = get_string('manager', 'role');
                    break;
                case 'coursecreator':
                    $original = get_string('coursecreators');
                    break;
                case 'editingteacher':
                    $original = get_string('defaultcourseteacher');
                    break;
                case 'teacher':
                    $original = get_string('noneditingteacher');
                    break;
                case 'student':
                    $original = get_string('defaultcoursestudent');
                    break;
                case 'guest':
                    $original = get_string('guest');
                    break;
                case 'user':
                    $original = get_string('authenticateduser');
                    break;
                case 'frontpage':
                    $original = get_string('frontpageuser', 'role');
                    break;
                // We should not get here, the role UI should require the name for custom roles!
                default:
                    $original = $ra->shortname;
                    break;
            }
        }
        $roles = $roles . $original . ', ';
    }

    $node = new core_user\output\myprofile\node('systemroles', 'role', get_string('role'), null, null,
        mb_strcut($roles, 1, -1));
    $tree->add_node($node);

    return true;
}

function local_smart_extend_navigation($navigation) {
    global $USER;

    if (!is_siteadmin($USER->id)) {
        $roles = [];
        foreach (get_user_roles(context_system::instance(), $USER->id) as $role) {
            $roles[$role->shortname] = $role->shortname;
        }
        $intersect = array_intersect(['manager', 'program_coordinator'], $roles);
        if (empty($intersect)) {
            return;
        }
    }

    $node = navigation_node::create(get_string('mycohorts', 'local_smart'),
        new moodle_url('/blocks/cohortnews/cohorts.php', ['scope'=>'my']),
        navigation_node::NODETYPE_LEAF,
        'mycohorts',
        'mycohorts',
        new pix_icon('t/addcontact', 'my')
    );
    $node->showinflatnavigation = true;
    $navigation->add_node($node);

    $node = navigation_node::create(get_string('allcohorts', 'local_smart'),
        new moodle_url('/blocks/cohortnews/cohorts.php', ['scope'=>'all']),
        navigation_node::NODETYPE_LEAF,
        'allcohorts',
        'allcohorts',
        new pix_icon('t/addcontact', 'my')
    );
    $node->showinflatnavigation = true;
    $navigation->add_node($node);

}

