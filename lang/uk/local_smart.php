<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Smart Local Plugin
 *
 * @package    local_smart
 * @author     Shilling <mitsserhiy@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2017 onwards Mits Serhiy (mitsserhiy@gmail.com)
 */

$string['title'] = 'Smart';
$string['pluginname'] = 'smart';
$string['smart'] = 'Smart';
$string['task_userfilter'] = 'Фільтрування користувачів за email';
$string['task_assignnotification'] = 'Сповіщення користувачів про нове завдання за допомогою email';

$string['usermanager'] = 'Управління користувачами';

$string['roleassignments'] = 'Просвоювання ролей';
$string['roleassignmentsdesc'] = 'Запустіть таск Просвоювання ролей, який виконає наступні операції:<br>
1) Присвоює роль Student всім користувачам з доменом @my.kmbs.ua.<br>
2) Видаляє усіх зайвих користувачів, які получені за допомогою синхронізації з MS Active Directory';

$string['o365sync'] = 'Синхронізація з MS Active Directory';
$string['o365syncdesc'] = 'Запустіть процес синхронізація користувачів з MS Azure. Зазвичай це завдання виконуєтсья за допомогою запланованих задач Cron, але тут можна запустити його з веб сторінки';

$string['userupload'] = 'Завантаження користувачів з CSV файлу';
$string['useruploaddesc'] = '1) Підготуйте CSV файл, який містить користувачів, яким будуть присвоєні ролі teacher, manager, course creators з наступними полями: "username,firstname,lastname,email,sysrole1,auth". В цей файл слід включати тільки вчителів, менеджерів та творців курсів, не включайте студентів.<br>
2) Кликніть посилання завантаження та дотримуйтесь інструкцій помічника. Вибирайте режим оновлення користувачів.';

$string['mycohorts'] = 'Мої групи';
$string['allcohorts'] = 'Усі групи';
