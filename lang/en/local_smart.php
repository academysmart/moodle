<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Smart Local Plugin
 *
 * @package    local_smart
 * @author     Shilling <mitsserhiy@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2017 onwards Mits Serhiy (mitsserhiy@gmail.com)
 */

/**
 * This file contains language strings used in the smart plugin
 *
 * @package local_smart
 */

$string['title'] = 'Smart';
$string['pluginname'] = 'smart';
$string['smart'] = 'Smart';
$string['task_userfilter'] = 'Filter users by email';
$string['task_assignnotification'] = 'Notify users about new task via email';

$string['usermanager'] = 'User management';

$string['roleassignments'] = 'Role assignments';
$string['roleassignmentsdesc'] = 'Run Role assignments task which perfom next operations:<br>
1) assign Student role to all users with @my.kmbs.ua domain in user name.<br>2) delete all extra users, which are obtained via synchronization with MS Active Directory';

$string['o365sync'] = 'Synchronization with MS Active Directory';
$string['o365syncdesc'] = 'Run this process to synchronize users from MS Azure. Usually this issue runs through sheduled, but running via web page is allow instead';

$string['userupload'] = 'Upload Users from CSV file';
$string['useruploaddesc'] = '1) Prepare CSV file containing role assignments for teachers, managers and course creators structured by following fields "username,firstname,lastname,email,sysrole1,auth": . You may put only teachers, managers and course creators into this file.<br>
2) Click link above and follow by upload wizard. Select user update mode.';

$string['mycohorts'] = 'My cohorts';
$string['allcohorts'] = 'All cohorts';