<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Smart Local Plugin
 *
 * @package    local_smart
 * @author     Shilling <mitsserhiy@gmail.com>, Jurets <jurets75@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2017 onwards Mits Serhiy (mitsserhiy@gmail.com), Yurii Hetmanskyi (jurets75@gmail.com)
 */

// Place common plugin code here instead file 'lib.php' , e.g.
//
//function some_function () {...}
//
// or
//
//class someClass {
//    public static function someMethod() { ... }
//}