<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Smart Local Plugin
 *
 * @package    local_smart
 * @author     Shilling <mitsserhiy@gmail.com>, Jurets <jurets75@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2017 onwards Mits Serhiy (mitsserhiy@gmail.com), Yurii Hetmanskyi (jurets75@gmail.com)
 */

require('../../config.php');

require_login();

$context = context_system::instance();
$str_title = get_string('usermanager', 'local_smart');

$return_url = new moodle_url('/my/index.php');
$url_str = '/local/smart/usermanager.php';
$baseurl = new moodle_url($url_str);

$task = optional_param('task', false, PARAM_TEXT); //if no courseid is given

$PAGE->set_context($context);
$PAGE->set_url($baseurl);
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('incourse');
$PAGE->set_title($str_title);
$PAGE->navbar->add($str_title);

echo $OUTPUT->header();
echo $OUTPUT->heading($str_title, 2);

if (!is_siteadmin()) {
    $notificationerror = get_string('accessdenied', 'admin');
}

if (!empty($notificationerror)) {
    echo $OUTPUT->notification($notificationerror);
} else if ($task !== FALSE) {
    \local_smart\taskrunner::run($task);
    echo html_writer::link($url_str, get_string('back'));
} else {
    $url_usersync = new moodle_url($url_str, ['task'=>'\local_o365\task\usersync']);
    echo html_writer::link($url_usersync, get_string('o365sync', 'local_smart'));
    echo html_writer::tag('p', get_string('o365syncdesc', 'local_smart'));

    echo '<hr><br>';

    $url_userfilter = new moodle_url($url_str, ['task'=>'\local_smart\task\userfilter']);
    echo html_writer::div(
        html_writer::link($url_userfilter, get_string('roleassignments', 'local_smart'))
    );
    echo html_writer::tag('p', get_string('roleassignmentsdesc', 'local_smart'));

    echo '<hr><br>';

    echo html_writer::link(new moodle_url('/admin/tool/uploaduser/index.php'), get_string('userupload', 'local_smart'));
    echo html_writer::tag('p', get_string('useruploaddesc', 'local_smart'));

}

echo $OUTPUT->footer();

